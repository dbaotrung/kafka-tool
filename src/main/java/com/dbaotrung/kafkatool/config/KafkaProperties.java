package com.dbaotrung.kafkatool.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "app.kafka")
public class KafkaProperties {
    private String bootstrapServers;
    private String awsSchemaRegistryRegion;
    private String awsSchemaRegistryRegistryName;
    private String autoRegisterSchema;
    private String testSchemaRegistryEnabled;
    private String schemaRegistryUrl;
    private String awsIamAuthentication;
    private String awsRoleArn;

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public String getAwsSchemaRegistryRegion() {
        return awsSchemaRegistryRegion;
    }

    public void setAwsSchemaRegistryRegion(String awsSchemaRegistryRegion) {
        this.awsSchemaRegistryRegion = awsSchemaRegistryRegion;
    }

    public String getAwsSchemaRegistryRegistryName() {
        return awsSchemaRegistryRegistryName;
    }

    public void setAwsSchemaRegistryRegistryName(String awsSchemaRegistryRegistryName) {
        this.awsSchemaRegistryRegistryName = awsSchemaRegistryRegistryName;
    }

    public String getAutoRegisterSchema() {
        return autoRegisterSchema;
    }

    public void setAutoRegisterSchema(String autoRegisterSchema) {
        this.autoRegisterSchema = autoRegisterSchema;
    }

    public String getTestSchemaRegistryEnabled() {
        return testSchemaRegistryEnabled;
    }

    public void setTestSchemaRegistryEnabled(String testSchemaRegistryEnabled) {
        this.testSchemaRegistryEnabled = testSchemaRegistryEnabled;
    }

    public String getSchemaRegistryUrl() {
        return schemaRegistryUrl;
    }

    public void setSchemaRegistryUrl(String schemaRegistryUrl) {
        this.schemaRegistryUrl = schemaRegistryUrl;
    }

    public String getAwsIamAuthentication() {
        return awsIamAuthentication;
    }

    public void setAwsIamAuthentication(String awsIamAuthentication) {
        this.awsIamAuthentication = awsIamAuthentication;
    }

    public String getAwsRoleArn() {
        return awsRoleArn;
    }

    public void setAwsRoleArn(String awsRoleArn) {
        this.awsRoleArn = awsRoleArn;
    }
}
