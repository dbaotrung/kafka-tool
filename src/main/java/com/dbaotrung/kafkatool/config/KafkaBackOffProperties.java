package com.dbaotrung.kafkatool.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.MaxAttemptsRetryPolicy;

@ConfigurationProperties(prefix = "app.kafka.back-off")
public class KafkaBackOffProperties {

    private int maxRetries = MaxAttemptsRetryPolicy.DEFAULT_MAX_ATTEMPTS - 1;
    private long initialInterval = ExponentialBackOffPolicy.DEFAULT_INITIAL_INTERVAL;
    private double multiplier = ExponentialBackOffPolicy.DEFAULT_MULTIPLIER;
    private long maxInterval = ExponentialBackOffPolicy.DEFAULT_MAX_INTERVAL;

    public int getMaxRetries() {
        return maxRetries;
    }

    public void setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    public long getInitialInterval() {
        return initialInterval;
    }

    public void setInitialInterval(long initialInterval) {
        this.initialInterval = initialInterval;
    }

    public double getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(double multiplier) {
        this.multiplier = multiplier;
    }

    public long getMaxInterval() {
        return maxInterval;
    }

    public void setMaxInterval(long maxInterval) {
        this.maxInterval = maxInterval;
    }
}
