package com.dbaotrung.kafkatool.config;

import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.CommonClientConfigs;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaAuthenticationConfiguration {

    private static final String SASL_MECHANISM_CONFIG = "sasl.mechanism";
    private static final String SASL_JAAS_CONFIG_CONFIG = "sasl.jaas.config";
    private static final String SASL_CLIENT_CALLBACK_HANDLER_CLASS_CONFIG = "sasl.client.callback.handler.class";

    @Bean("kafkaAuthenticationProperties")
    @ConditionalOnProperty(value = {"app.kafka.awsIamAuthentication"}, havingValue = "true", matchIfMissing = true
    )
    public Map<String, Object> kafkaAuthenticationProperties() {
        Map<String, Object> result = new HashMap<>();
        result.put(CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, "SASL_SSL");
        result.put(SASL_MECHANISM_CONFIG, "AWS_MSK_IAM");
        result.put(SASL_JAAS_CONFIG_CONFIG,
                "software.amazon.msk.auth.iam.IAMLoginModule required awsRoleArn=\"${kafkaProperties.awsRoleArn}\";");
        result.put(SASL_CLIENT_CALLBACK_HANDLER_CLASS_CONFIG, "software.amazon.msk.auth.iam.IAMClientCallbackHandler");
        return result;
    }

    @Bean("kafkaAuthenticationProperties")
    @ConditionalOnProperty(value = {"app.kafka.awsIamAuthentication"}, havingValue = "false")
    public Map<String, Object> kafkaWithoutAuthenticationProperties() {
        return new HashMap<>();
    }
}
