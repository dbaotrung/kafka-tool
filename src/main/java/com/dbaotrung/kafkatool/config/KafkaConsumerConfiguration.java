package com.dbaotrung.kafkatool.config;

import com.amazonaws.services.schemaregistry.deserializers.GlueSchemaRegistryKafkaDeserializer;
import com.amazonaws.services.schemaregistry.utils.AWSSchemaRegistryConstants;
import com.amazonaws.services.schemaregistry.utils.AvroRecordType;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.kafka.core.KafkaOperations;
import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
import org.springframework.kafka.listener.DefaultErrorHandler;
import org.springframework.kafka.support.ExponentialBackOffWithMaxRetries;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;

@EnableKafka
@Configuration
@EnableConfigurationProperties(value = {KafkaProperties.class, KafkaBackOffProperties.class})
public class KafkaConsumerConfiguration {

    private static final String AVRO_CONSUMER_PROPERTIES = "avroConsumerProperties";
    public static final String TEST_SCHEMA_REGISTER_URL = "mock://testUrl";
    private Logger logger = LoggerFactory.getLogger(getClass());

    private KafkaProperties kafkaProperties;
    private KafkaBackOffProperties backOffProperties;

    public KafkaConsumerConfiguration(KafkaProperties kafkaProperties,
            KafkaBackOffProperties backOffProperties) {
        this.kafkaProperties = kafkaProperties;
        this.backOffProperties = backOffProperties;
    }

    @Bean(AVRO_CONSUMER_PROPERTIES)
    public Map<String, Object> avroConsumerProperties() {
        Map<String, Object> result = new HashMap<>();
        result.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        result.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        result.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
//        result.put(ConsumerConfig.INTERCEPTOR_CLASSES_CONFIG, KafkaLoggingConsumerInterceptor.class);
        result.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ErrorHandlingDeserializer.class);

        return result;
    }

    @Bean
    @ConditionalOnProperty(value = {"app.kafka.testSchemaRegistryEnabled"}, havingValue = "false", matchIfMissing = true)
    public ConsumerFactory<String, Object> awsConsumerFactory(
            @Qualifier(AVRO_CONSUMER_PROPERTIES) Map<String, Object> avroConsumerProperties,
            @Qualifier("kafkaAuthenticationProperties") Map<String, Object> kafkaAuthenticationProperties
    ) {
        Map<String, Object> config = new HashMap<>(avroConsumerProperties);
        config.put(ErrorHandlingDeserializer.VALUE_DESERIALIZER_CLASS,
                GlueSchemaRegistryKafkaDeserializer.class.getName());
        config.put(AWSSchemaRegistryConstants.AWS_REGION, kafkaProperties.getAwsSchemaRegistryRegion());
        config.put(AWSSchemaRegistryConstants.AVRO_RECORD_TYPE, AvroRecordType.SPECIFIC_RECORD.getName());
        return new DefaultKafkaConsumerFactory<>(config);
    }

    @Bean
    @ConditionalOnProperty(value = {"app.kafka.testSchemaRegistryEnabled"}, havingValue = "true")
    public ConsumerFactory<String, Object> testConsumerFactory(
            @Qualifier(AVRO_CONSUMER_PROPERTIES) Map<String, Object> avroConsumerProperties,
            @Qualifier("kafkaAuthenticationProperties") Map<String, Object> kafkaAuthenticationProperties
    ) {
        Map<String, Object> config = new HashMap<>(avroConsumerProperties);
        config.put(ErrorHandlingDeserializer.VALUE_DESERIALIZER_CLASS,
                KafkaAvroDeserializer.class.getName());
        String testSchemaUrl = kafkaProperties.getSchemaRegistryUrl();
        if (testSchemaUrl == null) {
            testSchemaUrl = TEST_SCHEMA_REGISTER_URL;
        }
        config.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, testSchemaUrl);
        config.put(KafkaAvroDeserializerConfig.SPECIFIC_AVRO_READER_CONFIG, "true");
        return new DefaultKafkaConsumerFactory<>(config);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, Object> kafkaListenerContainerFactory(
            ConsumerFactory<String, Object> consumerFactory,
            DefaultErrorHandler defaultErrorHandler
    ) {
        ConcurrentKafkaListenerContainerFactory<String, Object> result = new ConcurrentKafkaListenerContainerFactory<>();
        result.setConsumerFactory(consumerFactory);
//        result.setRecordInterceptor(new KafkaLoggingRecordInterceptor((kafkaProperties.shouldLogPayload ?:false)))
        result.setCommonErrorHandler(defaultErrorHandler);
        return result;
    }

    @Bean
    public DefaultErrorHandler defaultErrorHandler(DeadLetterPublishingRecoverer deadLetterPublishingRecoverer) {
        ExponentialBackOffWithMaxRetries exponentialBackOff = new ExponentialBackOffWithMaxRetries(
                backOffProperties.getMaxRetries());
        exponentialBackOff.setInitialInterval(backOffProperties.getInitialInterval());
        exponentialBackOff.setMultiplier(backOffProperties.getMultiplier());
        exponentialBackOff.setMaxInterval(backOffProperties.getMaxInterval());
        DefaultErrorHandler defaultErrorHandler = new DefaultErrorHandler(deadLetterPublishingRecoverer,
                exponentialBackOff);
        return defaultErrorHandler;
    }

    @Bean
    public DeadLetterPublishingRecoverer deadLetterPublishingRecoverer(KafkaOperations<String, Object> template) {
        return new DeadLetterPublishingRecoverer(template);
    }

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        logger.info("Bootstrap servers: [{}]", kafkaProperties.getBootstrapServers());
        return new KafkaAdmin(configs);
    }
}