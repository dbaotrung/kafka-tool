package com.dbaotrung.kafkatool.config;

import com.amazonaws.services.schemaregistry.serializers.GlueSchemaRegistryKafkaSerializer;
import com.amazonaws.services.schemaregistry.utils.AWSSchemaRegistryConstants;
import io.confluent.kafka.serializers.AbstractKafkaAvroSerDeConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import software.amazon.awssdk.services.glue.model.DataFormat;

@EnableKafka
@Configuration
@EnableConfigurationProperties(value = {KafkaProperties.class})
public class KafkaProducerConfiguration {

    private static final String COMMON_PRODUCER_PROPERTIES = "commonProducerProperties";
    private static final String KAFKA_AUTHENTICATION_PROPERTIES = "kafkaAuthenticationProperties";

    private KafkaProperties kafkaProperties;

    public KafkaProducerConfiguration(KafkaProperties kafkaProperties) {
        this.kafkaProperties = kafkaProperties;
    }

    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate(
            @Autowired @Qualifier("producerProperties") Map<String, Object> producerFactory
    ) {
        return new KafkaTemplate(new DefaultKafkaProducerFactory(producerFactory));
    }

    @Bean(COMMON_PRODUCER_PROPERTIES)
    public Map<String, Object> commonProducerProperties() {
        Map<String, Object> result = new HashMap<>();
        result.put(CommonClientConfigs.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        result.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
//        result.put(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG, KafkaLoggingProducerInterceptor.class.getName());
//        result.put(SHOULD_LOG_PAYLOAD, kafkaProperties.getShouldLogPayload());
        return result;
    }

    @Bean("producerProperties")
    @ConditionalOnProperty(value = {"app.kafka.testSchemaRegistryEnabled"}, havingValue = "false", matchIfMissing = true)
    public Map<String, Object> awsProducerProperties(
            @Qualifier(COMMON_PRODUCER_PROPERTIES) Map<String, Object> commonProducerProperties,
            @Qualifier(KAFKA_AUTHENTICATION_PROPERTIES) Map<String, Object> kafkaAuthenticationProperties
    ) {
        Map<String, Object> result = new HashMap<>(commonProducerProperties);
        result.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, GlueSchemaRegistryKafkaSerializer.class.getName());
        result.put(AWSSchemaRegistryConstants.DATA_FORMAT, DataFormat.AVRO.name());
        result.put(AWSSchemaRegistryConstants.AWS_REGION, kafkaProperties.getAwsSchemaRegistryRegion());
        result.put(AWSSchemaRegistryConstants.REGISTRY_NAME, kafkaProperties.getAwsSchemaRegistryRegistryName());
        result.put(AWSSchemaRegistryConstants.SCHEMA_AUTO_REGISTRATION_SETTING,
                kafkaProperties.getAutoRegisterSchema());
        return result;
    }

    @Bean("producerProperties")
    @ConditionalOnProperty(value = {"app.kafka.testSchemaRegistryEnabled"}, havingValue = "true")
    public Map<String, Object> testProducerProperties(
            @Qualifier(COMMON_PRODUCER_PROPERTIES) Map<String, Object> commonProducerProperties
    ) {
        Map<String, Object> result = new HashMap<>(commonProducerProperties);
        result.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
        String schemaUrl = kafkaProperties.getSchemaRegistryUrl();
        if (schemaUrl == null) {
            schemaUrl = KafkaConsumerConfiguration.TEST_SCHEMA_REGISTER_URL;
        }
        result.put(AbstractKafkaAvroSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG, schemaUrl);
        result.put(AWSSchemaRegistryConstants.SCHEMA_AUTO_REGISTRATION_SETTING,
                kafkaProperties.getAutoRegisterSchema());
        return result;
    }
}
