package com.dbaotrung.kafkatool.controllers;

import com.dbaotrung.kafkatool.services.KafkaTopicService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaTopicController {

    @Autowired
    private KafkaTopicService kafkaTopicService;

    public List<String> listTopics() {
        return kafkaTopicService.listTopics();
    }

}
